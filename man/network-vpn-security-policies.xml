<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS/DTD DocBook XML V4.2//EN"
	"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">

<refentry id="network-zone">
	<refentryinfo>
		<title>network-vpn-security-policies</title>
		<productname>network</productname>

		<authorgroup>
			<author>
				<contrib>Developer</contrib>
				<firstname>Michael</firstname>
				<surname>Tremer</surname>
				<email>michael.tremer@ipfire.org</email>
			</author>
		</authorgroup>
	</refentryinfo>

	<refmeta>
		<refentrytitle>network-vpn-security-policies</refentrytitle>
		<manvolnum>8</manvolnum>
	</refmeta>

	<refnamediv>
		<refname>network-vpn-security-policies</refname>
		<refpurpose>Network Configuration Control Program</refpurpose>
	</refnamediv>

	<refsynopsisdiv>
		<cmdsynopsis>
			<command>network vpn security-policies <arg choice="plain">[new|destroy]</arg> <replaceable>NAME</replaceable> ...</command>
		</cmdsynopsis>

		<cmdsynopsis>
			<command>network vpn security-policies <replaceable>NAME</replaceable> <arg choice="plain">command</arg> ...</command>
		</cmdsynopsis>
	</refsynopsisdiv>

	<refsect1>
		<title>Description</title>

		<para>
			With help of the <command>vpn security-policies</command>, it is possible
			to create, destroy and edit VPN security policies.
		</para>
		<para>
			A security policy is a definition of ciphers and algorithms for integrity
			and key-exchanges for VPN connections.
		</para>
	</refsect1>

	<refsect1>
		<title>Commands</title>

		<para>
			The following commands are understood:
		</para>

		<variablelist>
			<varlistentry>
				<term>
					<command>new <replaceable>NAME</replaceable></command>
				</term>

				<listitem>
					<para>
						A new security policy may be created with the
						<command>new</command> command.
					</para>

					<para>
						<replaceable>NAME</replaceable> does not allow any spaces.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command>destroy <replaceable>NAME</replaceable></command>
				</term>

				<listitem>
					<para>
						A security policy can be destroyed with this command.
					</para>
					<para>
						If the policy is still in use, it cannot be deleted.
					</para>
				</listitem>
			</varlistentry>
		</variablelist>

		<para>
			For all other commands, the name of the security policy needs to be passed first:
		</para>

		<variablelist>
			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> show</command>
				</term>

				<listitem>
					<para>
						Shows the configuration of the security policy.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> key-exchange <replaceable>[IKEv2|IKEv1]</replaceable></command>
				</term>

				<listitem>
					<para>
						Defines the key exchange algorithm that should be used to
						initiate an IPsec VPN connection.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> ciphers <replaceable>[CIPHER-LIST|+CIPHER ...|-CIPHER ...]</replaceable></command>
				</term>

				<listitem>
					<para>
						This command allows modifying the cipher list.
					</para>

					<para>
						A new <replaceable>CIPHER-LIST</replaceable> can be passed
						which will replace the current configuration.
						Alternatively, new ciphers can be added by prepending a
						+ sign to the cipher name and can removed likewise
						using -.
					</para>

					<para>
						A cipher is an algorithm that encrypts and decrypts data
						to be able to transmit it over an insecure channel.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> integrities <replaceable>[INTEGRITY-LIST|+INTEGRITY ...|-INTEGRITY ...]</replaceable></command>
				</term>

				<listitem>
					<para>
						This command allows modifying the integrity list
						similar to the <command>ciphers</command> command.
					</para>

					<para>
						Integrity algorithms are used to be able to determine
						if data has been altered when being transfered over
						an untrusted channel.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> group-types <replaceable>[GROUP-TYPES-LIST|+GROUP-TYPE ...|-GROUP-TYPE]</replaceable>
					</command>
				</term>

				<listitem>
					<para>
						This command allows modifying the list of group types
						similar to the <command>ciphers</command> command.
					</para>

					<para>
						These algorithms are used to negotiate a shared secret
						of an insecure channel.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> pfs <replaceable>[on|off]</replaceable></command>
				</term>

				<listitem>
					<para>
						This command allows to enable or disable Perfect Forward Secrecy (PFS).
					</para>

					<para>
						If PFS is enabled, the encrypted channels of a VPN connection will be
						renegotiated regularly to avoid that the same keys are used for too long.
						If an attacker is able to obtain a key that was used to encrypt the
						data, it is only possible to decrypt a certain amount of data.
					</para>

					<para>
						It is strongly recommended to enable PFS at all times.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> lifetime <replaceable>LIFETIME</replaceable></command>
				</term>

				<listitem>
					<para>
						This command allows to define how often the VPN connection is
						renegotiated if PFS is enabled.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<command><replaceable>NAME</replaceable> compression <replaceable>[on|off]</replaceable></command>
				</term>

				<listitem>
					<para>
						This command allows to enable or disable compression.
					</para>

					<para>
						If compression is enabled, all data is being compressed before being
						sent through the VPN.
						This setting is ignored if the peer does not support this.
					</para>
				</listitem>
			</varlistentry>
		</variablelist>
	</refsect1>

	<refsect1>
		<title>System Policies</title>

		<para>
			The system comes with builtin policies that cannot be modified by the user.
			They are intended to provide good defaults for various situations.
		</para>

		<refsect2>
			<title>system</title>

			<para>
				This policy is the default for every VPN connection and allows using
				all ciphers, integrity and key-exchange algorithms that are recommended
				to use and have not been proven or assumed to be broken, yet.
			</para>

			<para>
				Over time, this policy will change whenever an algorithm has been broken
				and is not recommended to be used any more.
			</para>
		</refsect2>

		<refsect2>
			<title>performance</title>

			<para>
				This policy is recommended to be used on systems that are not very powerful.
				Algorithms with smaller key lengths, but still considered to be secure
				are being used.
			</para>
		</refsect2>

		<para>
			System policies cannot be deleted.
		</para>
	</refsect1>

	<refsect1>
		<title>See Also</title>

		<para>
			<citerefentry>
				<refentrytitle>network</refentrytitle>
				<manvolnum>8</manvolnum>
			</citerefentry>,
			<citerefentry>
				<refentrytitle>network-vpn</refentrytitle>
				<manvolnum>8</manvolnum>
			</citerefentry>
		</para>
	</refsect1>
</refentry>
