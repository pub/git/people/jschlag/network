<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS/DTD DocBook XML V4.2//EN"
	"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">

<refentry id="network-zone-pppoe">
	<refentryinfo>
		<title>network-zone-pppoe</title>
		<productname>network</productname>

		<authorgroup>
			<author>
				<contrib>Developer</contrib>
				<firstname>Michael</firstname>
				<surname>Tremer</surname>
				<email>michael.tremer@ipfire.org</email>
			</author>
		</authorgroup>
	</refentryinfo>

	<refmeta>
		<refentrytitle>network-zone-pppoe</refentrytitle>
		<manvolnum>8</manvolnum>
	</refmeta>

	<refnamediv>
		<refname>network-zone-pppoe</refname>
		<refpurpose>Network Configuration Control Program</refpurpose>
	</refnamediv>

	<refsynopsisdiv>
		<cmdsynopsis>
			<command>network zone new <replaceable>ZONE</replaceable> pppoe ...</command>
		</cmdsynopsis>
	</refsynopsisdiv>

	<refsect1>
		<title>Description</title>

		<para>
			The pppoe hook creates a PPPoE connection to your ISP.
		</para>
	</refsect1>

	<refsect1>
		<title>Options</title>

		<para>
			The following options are understood:
		</para>

		<variablelist>

			<varlistentry>
				<term>
					<option>--username=<replaceable>USERNAME</replaceable></option>
				</term>

				<listitem>
					<para>
						Sets the username for authentication.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--password=<replaceable>PASSWORD</replaceable></option>
				</term>

				<listitem>
					<para>
						Sets the password for authentication.
					</para>
					<para>
						Use the <option>--auth=</option> option to transmit it
						in a secure manner to the provider.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--mtu=<emphasis>N</emphasis></option>
				</term>

				<listitem>
					<para>
						Sets the default MTU of the PPP connection.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--auth=[chap|pap]</option>
				</term>

				<listitem>
					<para>
						Define the authentication method that is used to
						authenticate against your provider.
						The default is to use the provider's preference.
					</para>
					<itemizedlist>
						<listitem>
							<para>
								<emphasis>Challange-Handshake Authentication Protocol</emphasis>
								(chap) is the preferred secure method.
							</para>
						</listitem>
						<listitem>
							<para>
								<emphasis>Password Authentication Protocol</emphasis>
								(pap) sends the plaintext password to the authentication
								server which is the reason why it should be avoided to use PAP.
							</para>
						</listitem>
					</itemizedlist>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--access-concentrator=<replaceable>STRING</replaceable></option>
				</term>

				<listitem>
					<para>
						By this option, you may define the name of the access concentrator.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--service-name=<replaceable>STRING</replaceable></option>
				</term>

				<listitem>
					<para>
						By this option, you may define the service name.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--ipv6=[<emphasis>on</emphasis>|off]</option>
				</term>

				<listitem>
					<para>
						By this option, you may enable or disable IPv6
					</para>				</listitem>
			</varlistentry>

			<varlistentry>
				<term>
					<option>--prefix-delegation=[<emphasis>on</emphasis>|off]</option>
				</term>

				<listitem>
					<para>
						 By this option, you may enable or disable the delegation through your provider of one IPv6 prefix to your system.
					</para>
				</listitem>
			</varlistentry>

		</variablelist>
	</refsect1>

	<refsect1>
		<title>See Also</title>

		<para>
			<citerefentry>
				<refentrytitle>network</refentrytitle>
				<manvolnum>8</manvolnum>
			</citerefentry>,
			<citerefentry>
				<refentrytitle>network-zone</refentrytitle>
				<manvolnum>8</manvolnum>
			</citerefentry>
		</para>
	</refsect1>
</refentry>
