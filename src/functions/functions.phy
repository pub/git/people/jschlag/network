#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

PHY_DIR="/sys/class/ieee80211"

phy_dir() {
	local phy=${1}

	echo "${PHY_DIR}/${phy}"
}

phy_exists() {
	local phy=${1}
	assert isset phy

	[ -d "$(phy_dir ${phy})" ]
}

phy_list() {
	list_directory "$(phy_dir)"
}

phy_get() {
	local info="${1}"
	local phy

	# As this is already a valid phy, we don't need to search on...
	if phy_exists "${info}"; then
		print "${info}"
		return ${EXIT_OK}
	fi

	# If this is an existing device, we can figure out the name of the
	# phy directly.
	if device_exists ${info}; then
		phy="$(device_get_phy ${info})"

	# If we just got the MAC address we need to walk though all
	# available phys and find the right one.
	elif mac_is_valid ${info}; then
		local i
		for i in $(phy_list); do
			if [ "${info}" = "$(phy_get_address ${i})" ]; then
				phy=${i}
				break
			fi
		done
	fi

	log DEBUG "Searching for phy = ${info}, found ${phy:-none}"

	if [ -z "${phy}" ]; then
		return ${EXIT_ERROR}
	fi

	echo "${phy}"
	return ${EXIT_OK}
}

phy_get_address() {
	local phy=${1}
	assert isset phy

	local path="$(phy_dir ${phy})/macaddress"
	[ -r "${path}" ] || return ${EXIT_ERROR}

	print "$(<${path})"
}

phy_get_devices() {
	local phy="${1}"
	assert isset phy

	local device
	for device in $(device_list); do
		local p="$(device_get_phy "${device}")"

		if [ "${phy}" = "${p}" ]; then
			print "${device}"
		fi
	done
}
