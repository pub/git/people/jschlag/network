#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

. /usr/lib/network/header-port

HOOK_SETTINGS="HOOK ADDRESS PARENT_DEVICE TAG"

PORT_PARENTS_VAR="PARENT"

hook_check_settings() {
	assert isset PARENT_DEVICE
	assert isinteger TAG

	if isset ADDRESS; then
		assert ismac ADDRESS
	fi

	if [ ${TAG} -gt 4096 ]; then
		error "TAG is greater than 4096."
		exit ${EXIT_ERROR}
	fi

	local reserved
	for reserved in 0 4095; do
		if [ "${TAG}" = "${reserved}" ]; then
			error "TAG=${reserved} is reserved."
			exit ${EXIT_ERROR}
		fi
	done
}

hook_new() {
	while [ $# -gt 0 ]; do
		case "${1}" in
			--parent-device=*)
				PARENT_DEVICE=$(cli_get_val "${1}")
				;;
			--address=*)
				ADDRESS=$(cli_get_val "${1}")
				;;
			--tag=*)
				TAG=$(cli_get_val "${1}")
				;;
			*)
				warning "Unknown argument '${1}'"
				;;
		esac
		shift
	done

	local port="${PARENT_DEVICE}${VLAN_PORT_INTERFIX}${TAG}"

	port_settings_write "${port}" ${HOOK_SETTINGS}

	exit ${EXIT_OK}
}

hook_edit() {
	local port=${1}
	assert isset port
	shift

	port_settings_read "${port}" ${HOOK_SETTINGS}

	while [ $# -gt 0 ]; do
		case "${1}" in
			--address=*)
				ADDRESS=$(cli_get_val "${1}")
				;;
			*)
				warning "Unknown argument '${1}'"
				;;
		esac
		shift
	done

	port_settings_write "${port}" ${HOOK_SETTINGS}

	exit ${EXIT_OK}	
}

hook_create() {
	local port="${1}"
	assert isset port

	device_exists "${port}" && exit ${EXIT_OK}

	# Read configruation
	port_settings_read "${port}" ${HOOK_SETTINGS}

	# Create the VLAN device
	vlan_create "${port}" "${PARENT_DEVICE}" "${TAG}" "${ADDRESS}"

	exit ${EXIT_OK}
}

hook_remove() {
	local port="${1}"
	assert isset port

	if device_exists "${port}"; then
		vlan_remove "${port}"
	fi

	exit ${EXIT_OK}
}
